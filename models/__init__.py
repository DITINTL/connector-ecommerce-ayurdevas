# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from . import ayurdevas_backend
from . import ayurdevas_binding
from . import sale_order
from . import partner
