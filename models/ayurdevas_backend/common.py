# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).


from odoo import fields, models


class AyurdevasBackend(models.Model):
    _name = 'ayurdevas.backend'
    _description = 'Ayurdevas Ecommerce Backend'
    _inherit = 'connector.backend'

    name = fields.Char()

    location = fields.Char(
        string='Location',
        required=True,
        help='Url to Ayurdevas ecommerce application',
    )

    access_token = fields.Char(
        string='Access Token',
        help='Token for web server authentication',
    )

    import_orders_from_date = fields.Datetime(
        string='Import sale orders from date',
        help='do not consider non-imported sale orders before this date. '
             'Leave empty to import all sale orders',
    )

    no_sales_order_sync = fields.Boolean(
        string='No Sale Orders Synchronization',
        help='Check if sale orders should not be imported',
    )

