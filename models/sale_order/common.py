# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import models, fields
from odoo.addons.component.core import Component


class AyurdevasSaleOrder(models.Model):
    _name = 'ayurdevas.sale.order'
    _inherit = 'ayurdevas.binding'
    _description = 'Ayurdevas Ecommerce Sale Order'
    _inherits = {'sale.order': 'odoo_id'}

    odoo_id = fields.Many2one(
        comodel_name='sale.order',
        string='Sale Order',
        required=True,
        ondelete='cascade'
    )


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    ayurdevas_bind_ids = fields.One2many(
        comodel_name='ayurdevas.sale.order',
        inverse_name='odoo_id',
        string='Ayurdevas Bindings',
        copy=False,
    )


class SaleOrderAdapter(Component):
    _name = 'ayurdevas.sale.order.adapter'
    _inherit = 'ayurdevas.adapter'
    _apply_on = 'ayurdevas.sale.order'
