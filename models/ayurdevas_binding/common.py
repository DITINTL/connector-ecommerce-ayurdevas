# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import api, fields, models
from odoo.addons.queue_job.job import job, related_action


class AyurdevasBinding(models.AbstractModel):
    """ Abstract Model for the Bindings.

    All the models used as bindings between Ayurdevas and Odoo
    (``ayurdevas.res.partner``, ``ayurdevas.product.product``, ...) should
    ``_inherit`` it.
    """
    _name = 'ayurdevas.binding'
    _inherit = 'external.binding'
    _description = 'Ayurdevas Ecommerce Binding (abstract)'

    # odoo_id = odoo-side id must be declared in concrete model
    backend_id = fields.Many2one(
        comodel_name='ayurdevas.backend',
        string='Ayurdevas Backend',
        required=True,
        ondelete='restrict',
    )

    external_id = fields.Char(string='ID on Ayurdevas ecommerce', index=True)

    _sql_constraints = [
        ('ayurdevas_uniq', 'unique(backend_id, external_id)',
         'A binding already exists with the same Ayurdevas ecommerce ID.'),
    ]

    @job(default_channel='root.ayurdevas')
    @related_action(action='related_action_unwrap_binding')
    @api.multi
    def export_record(self, fields=None):
        """ Export a record on Ayurdevas Ecommerce """
        self.ensure_one()
        with self.backend_id.work_on(self._name) as work:
            exporter = work.component(usage='record.exporter')
            return exporter.run(self, fields)
