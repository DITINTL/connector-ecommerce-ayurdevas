# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo.addons.component.core import Component
from odoo.addons.component_event import skip_if


class AyurdevasPartnerExportListener(Component):
    _name = 'ayurdevas.partner.export.listener'
    _inherit = 'base.connector.listener'
    _apply_on = ['res.partner']

    def on_record_write(self, record, fields=None):
        for binding in record.ayurdevas_bind_ids:
            binding.with_delay().export_record()

    def on_record_create(self, record, fields=None):
        """
        Create a ``ayurdevas.res.partner`` record. This record will then
        be exported to Ayurdevas Ecommerce.
        """
        for backend_id in self.env['ayurdevas.backend'].search([]):
            self.env['ayurdevas.res.partner'].create({
                'backend_id': backend_id.id,
                'odoo_id': record.id
            })


class AyurdevasBindingPartnerExportListener(Component):
    _name = 'ayurdevas.binding.partner.export.listener'
    _inherit = 'base.event.listener'
    _apply_on = ['ayurdevas.res.partner']

    def on_record_create(self, record, fields=None):
        record.with_delay().export_record()
