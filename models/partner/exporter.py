# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo.addons.component.core import Component
from odoo.addons.connector.components.mapper import changed_by,mapping,only_create


class ResPartnerExporter(Component):
    """ Export Partner to Ayurdevas Ecommerce """
    _name = 'ayurdevas.res.partner.exporter'
    _inherit = 'ayurdevas.base.exporter'
    _apply_on = 'ayurdevas.res.partner'

    def _validate_create_data(self, data):
        """ Check if the values to import are correct

        Pro-actively check before the ``Model.create`` or
        ``Model.update`` if some fields are missing

        Raise `InvalidDataError`
        """
        if not(data.get('categoria') and data.get('presea_tipo')):
            raise InvalidDataError("Sales team is not defined or invalid")
        return


class ResPartnerExportMapper(Component):
    _name = 'ayurdevas.partner.export.mapper'
    _inherit = 'ayurdevas.export.mapper'
    _apply_on = ['ayurdevas.res.partner']

    direct = [
        ('ref', 'username'),
        ('name','presea_n_fantasia'),
        ('email', 'email'),
        ('vat', 'presea_dni'),
    ]

    @changed_by('active')
    @mapping
    def confirmed(self, record):
        return {'confirmed': not record.active}

    @changed_by('property_account_position_id')
    @mapping
    def presea_iva(self, record):
        excluded_account = self.env.ref('l10n_ar_afip_tables.account_fiscal_position_cliente_ext')

        account_position_ref = None
        if record.property_account_position_id:
            account_position_ref = self.env.ref(record.property_account_position_id)

        tax_exclusion = False
        if account_position_ref == excluded_account:
            tax_exclusion = True
        return {'presea_iva': tax_exclusion}

    @only_create
    @mapping
    def password(self, record):
        # Deberia ir en direct mapping
        # return {'presea_clave': record.web_password}
        return {'presea_clave': '12345678'}

    @changed_by('team_id')
    @mapping
    def categoria(self, record):
        cat = ''
        sales_team_ref = self._get_sales_team_ref(record)

        if sales_team_ref in ['sales_team.team_dn','sales_team.team_dnp']:
            cat = 'MULT'
        elif sales_team_ref == 'sales_team.team_venta_directa':
            cat = 'VTADTA'
        elif sales_team_ref == 'sales_team.team_venta_directa_point':
            cat = 'LIDERES'
        elif sales_team_ref in ['sales_team.team_franquicia','sales_team.team_empleados']:
            cat = 'FRANQ'
        elif sales_team_ref == 'sales_team.team_nuevo_point':
            cat = 'NUEVOPOINT'
        elif sales_team_ref == 'sales_team.team_point':
            cat = 'POINT'
        return {'categoria': cat}

    @changed_by('ref')
    @mapping
    def presea_franquicia(self, record):
        num = 99999
        if record.ref and record.ref.isdigit():
            num = int(record.ref)
        return {'presea_franquicia': num < 11000 }

    @changed_by('team_id')
    @mapping
    def presea_tipo(self, record):
        tipo = 0
        sales_team_ref = self._get_sales_team_ref(record)

        if sales_team_ref in ['sales_team.team_dn','sales_team.team_dnp','sales_team.team_empleados']:
            tipo = 1
        elif sales_team_ref == 'sales_team.team_point':
            tipo = 2
        elif sales_team_ref in ['sales_team.team_venta_directa', 'sales_team.team_venta_directa_point']:
            tipo = 3
        elif sales_team_ref == 'sales_team.team_franquicia':
            tipo = 4
        return {'presea_tipo':tipo}

    @changed_by('sponsor_id')
    @mapping
    def compute_red(self, record):
        red = {
            'presea_red': '',
            'presea_red2': '',
            'presea_red3': ''
        }
        if record.sponsor_id:
            red['presea_red3'] = record.sponsor_id.ref
        return red

    def _get_sales_team_ref(self, record):
        return self.env.ref(record.team_id) if record.team_id else None
