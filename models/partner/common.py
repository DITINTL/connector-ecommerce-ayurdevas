# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import fields, models
from odoo.addons.component.core import Component


class AyurdevasResPartner(models.Model):
    _name = 'ayurdevas.res.partner'
    _inherit = 'ayurdevas.binding'
    _description = 'Ayurdevas Ecommerce Partner'
    _inherits = {'res.partner': 'odoo_id'}

    odoo_id = fields.Many2one(
        comodel_name='res.partner',
        string='Partner',
        required=True,
        ondelete='cascade'
    )


class ResPartner(models.Model):
    _inherit = 'res.partner'

    ayurdevas_bind_ids = fields.One2many(
        comodel_name='ayurdevas.res.partner',
        inverse_name='odoo_id',
        string='Ayurdevas Bindings',
    )


class PartnerAdapter(Component):
    _name = 'ayurdevas.partner.adapter'
    _inherit = 'ayurdevas.adapter'
    _apply_on = 'ayurdevas.res.partner'

    def read(self):
        return { 'updated_at': None }

    def create(self, *args, **kwargs):
        """ Create a record on the external system """
        return self._rand()

    def write(self, *args, **kwargs):
        """ Update records on the external system """
        return self._rand()

    def _rand(self):
        import random
        return random.randint(1,100000)
