# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    'name': 'Conector para ecommerce propio',
    'version': '12.0.0.0.1',
    'category': 'Connector',
    'summary': 'Conector para web',
    'description': 'Permite conexion de dos vias para pedidos, clientes, etc',
    'depends': [
        'connector',
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/ayurdevas_backend_views.xml',
        'views/connector_ayurdevas_menu.xml',
    ],
    'installable': True,
    'author': 'Roberto Sierra',
    'website': 'https://gitlab.com/ayurdevas/connector-ecommerce-ayurdevas',
    'license': 'AGPL-3',
}
