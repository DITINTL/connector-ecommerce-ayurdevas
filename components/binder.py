# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo.addons.component.core import Component


class AyurdevasModelBinder(Component):
    """ Bind records and give odoo/ayurdevas ids correspondence

    Binding models are models called ``ayurdevas.{normal_model}``,
    like ``ayurdevas.res.partner`` or ``ayurdevas.product.product``.
    They are ``_inherits`` of the normal models and contains
    the Ayurdevas ID, the ID of the Ayurdevas Backend and the additional
    fields belonging to the ayurdevas instance.
    """
    _name = 'ayurdevas.binder'
    _inherit = ['base.binder', 'ayurdevas.base']
    _apply_on = [
        'ayurdevas.res.partner',
    ]
