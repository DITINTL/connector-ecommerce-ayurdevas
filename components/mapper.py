# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo.addons.component.core import AbstractComponent


class AyurdevasExportMapper(AbstractComponent):
    _name = 'ayurdevas.export.mapper'
    _inherit = ['ayurdevas.base', 'base.export.mapper']
    _usage = 'export.mapper'
