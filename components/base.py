# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo.addons.component.core import AbstractComponent


class BaseAyurdevasConnectorComponent(AbstractComponent):
    """Base Ayurdevas ecommerce Connector Component

    All components of this connector should inherit from it.
    """
    _name = 'ayurdevas.base'
    _inherit = 'base.connector'
    _collection = 'ayurdevas.backend'
