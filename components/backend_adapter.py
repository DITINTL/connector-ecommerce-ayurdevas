# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo.addons.component.core import Component

AYURDEVAS_DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'


class AyurdevasAdapter(Component):
    """ Generic adapter for using the Ayurdevas backend """
    _name = 'ayurdevas.adapter'
    _inherit = ['base.backend.adapter.crud', 'ayurdevas.base']
    _usage = 'backend.adapter'
